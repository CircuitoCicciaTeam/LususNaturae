﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterState))]
public class IA_Tiger : IAActionSender
{

    public Transform target = null;

    public float distanceFromTargetForAttackBehaviour = 5f;
    public float minimumDistanceFromTargetAllowed = 2f;
    public float deltaTimeToAttemptAttackAction = 1f;
    public float timeForAfterDashPause = 3f;

    private float timeProgressionAttack = 0;
    private float timeProgressionAfterDashPause = 100; //start high to avoid initial pause effect

    public void Update()
    {
        if(target)
        {
            ProcessIA();
        }
        else
        {
            // Target was lost, stop acting
            this.enabled = false;
        }
       
    }

    public override void ProcessIA()
    {

        if(WaitForAfterDashPause())
        {
            return;
        }

        LookTowardsTarget();

        if (!UpdateAttackRoutine())
        {
            UpdateMoveRoutine();
        }
        

    }

    private bool WaitForAfterDashPause()
    {
        if(charState.IsUsingPower)
        {
            timeProgressionAfterDashPause = 0;
        }
        else
        {
            timeProgressionAfterDashPause += Time.deltaTime;
        }

        
        return timeProgressionAfterDashPause < timeForAfterDashPause;        

    }

    private void UpdateMoveRoutine()
    {
        if(Vector3.Distance(target.position, transform.position) > minimumDistanceFromTargetAllowed)
        {
            if (OnMoveLeft != null && charState.LookingDirection==Vector3.left)
            {
                OnMoveLeft.Invoke(EButtonAction.PRESSED);
            }
            else if (OnMoveRight != null && charState.LookingDirection == Vector3.right)
            {
                OnMoveRight.Invoke(EButtonAction.PRESSED);
            }

        }
    }

    private bool UpdateAttackRoutine()
    {

        timeProgressionAttack += Time.deltaTime;

        if (timeProgressionAttack >= deltaTimeToAttemptAttackAction)
        {
            timeProgressionAttack -= deltaTimeToAttemptAttackAction;

            if(Vector3.Distance(target.position, transform.position) > distanceFromTargetForAttackBehaviour)
            {
                //Dash from distance
                if(OnPower!=null)
                {
                    OnPower.Invoke(EButtonAction.DOWN);
                }

            }
            else
            {
                //Attack from close range
                if (OnAttack != null)
                {
                    OnAttack.Invoke(EButtonAction.DOWN);
                }
            }

            return true;

        }

        return false;
    }

    public void LookTowardsTarget()
    {
        float lookDirection = target.position.x - transform.position.x;
        if(lookDirection!=0)
        {
            charState.LookingDirection = Vector3.right * (lookDirection / Mathf.Abs(lookDirection));
        }
    }
}
