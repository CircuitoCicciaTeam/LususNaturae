﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DashAction : CharacterAction
{
    [Header("Settings")]
    public float distanceCovered = 1;
    public float animationDuration = 1;
    [Range(0f,1f)]
    public float startMovingAtNormalizedime = 0;

    public override void Execute(EButtonAction typeOfAction)
    {
        if (typeOfAction==EButtonAction.DOWN && !charState.IsUsingPower && !charState.IsAttacking)
        {
            StartCoroutine(CoroutineDash());
            charState.charAnimator.SetTrigger(AnimatorParameters.Dash);
        }
    }

    public IEnumerator CoroutineDash()
    {
        charState.IsUsingPower = true;
        float dashSpeed = distanceCovered / (animationDuration - animationDuration * startMovingAtNormalizedime);
        float timeProgression = Time.time;

        yield return new WaitForSeconds(animationDuration * startMovingAtNormalizedime);

        timeProgression = Time.time - timeProgression;

        charState.moveDirection.x += charState.LookingDirection.x * dashSpeed * (timeProgression - animationDuration * startMovingAtNormalizedime);

        while (timeProgression < animationDuration)
        { 
            yield return null;
            timeProgression += Time.deltaTime;
            charState.moveDirection.x += charState.LookingDirection.x * dashSpeed * Time.deltaTime;
        }

        charState.IsUsingPower = false;
    }

}
