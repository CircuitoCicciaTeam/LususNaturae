﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AnimatorParameters
{
    public static int Attack = Animator.StringToHash("Attack");
    public static int GetHit = Animator.StringToHash("GetHit");
    public static int LookingLeft = Animator.StringToHash("LookingLeft");
    public static int Jump = Animator.StringToHash("Jump");
    public static int Idle = Animator.StringToHash("Idle");
    public static int Grounded = Animator.StringToHash("Grounded");
    public static int Dash = Animator.StringToHash("Dash");
    public static int Block = Animator.StringToHash("Block");
}


