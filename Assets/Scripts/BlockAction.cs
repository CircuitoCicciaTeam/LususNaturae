﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAction : CharacterAction
{
    public ReceiveDamageAction damageablePart = null;

    public override void Execute(EButtonAction typeOfAction)
    {
        if(typeOfAction!=EButtonAction.NONE && !charState.IsAttacking)
        {
            charState.IsUsingPower = true;
            charState.charAnimator.SetBool(AnimatorParameters.Block, true);
            damageablePart.isInvincible = true;
        }
        else
        {
            charState.IsUsingPower = false;
            charState.charAnimator.SetBool(AnimatorParameters.Block, false);
            damageablePart.isInvincible = false;
        }
    }
}
