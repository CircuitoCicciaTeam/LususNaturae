﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ReceiveDamageAction : MonoBehaviour
{
    public Renderer compRendToFlicker = null;

    public Slider compSliderHealtBar = null;

    public UnityEvent OnDamageTaken;
    public UnityEvent OnDamageNullified;
    public UnityEvent OnHealtPointsDepleted;

    //public LayerMask layersTriggeringDamage = 0;

    public int healthPoints = 0;

    public float totalFlickerTime = 1;
    public float deltaFlickerTime = 0.2f;

    private int startingHealthPoints = 1;

    public bool isInvincible = false;

    public bool isFlickering = false;

    private void Awake()
    {
        startingHealthPoints = healthPoints;
    }

    private void Update()
    {
        if(compSliderHealtBar)
        {
            compSliderHealtBar.value = healthPoints / (float)startingHealthPoints;
        }
    }

    internal void ApplyDamage(int rawDamage)
    {
        if (!isInvincible /*&& !isFlickering*/)
        {
            Debug.Log(gameObject.name + " recived " + rawDamage + " damage");
            healthPoints -= rawDamage;
            healthPoints = Mathf.Max(healthPoints, 0);

            if(OnDamageTaken!=null)
            {
                OnDamageTaken.Invoke();
            }
        }
        else
        {
            if (OnDamageNullified != null)
            {
                OnDamageNullified.Invoke();
            }
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if((layersTriggeringDamage.value & other.gameObject.layer)!=0)
    //    {
    //        StartCoroutine(CoroutineFlickerAndDisable());
    //    }
    //}

    public IEnumerator CoroutineFlickerAndDisable()
    {
        if (!isInvincible/*isFlickering*/)
        {
            isInvincible = true;
            //isFlickering = true;

            float timeProgression = 0f;
            while (timeProgression < totalFlickerTime)
            {
                compRendToFlicker.enabled = Mathf.FloorToInt(timeProgression / deltaFlickerTime) % 2 != 0;
                yield return null;
                timeProgression += Time.deltaTime;
            }

            compRendToFlicker.enabled = true;

            if (healthPoints <= 0 && OnHealtPointsDepleted != null)
            {
                OnHealtPointsDepleted.Invoke();
            }

            isInvincible = false;
            //isFlickering = false;
        }
    }

}
