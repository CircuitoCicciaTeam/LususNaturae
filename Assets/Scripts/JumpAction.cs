﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JumpAction : CharacterAction
{
    [Header("Settings")]
    public float jumpSpeed = 1;
    [Tooltip("Affects gravity after reaching the top peack of the jump")]
    public float fallMultiplier = 1;
    [Tooltip("Affects gravity while the jump is going to the peack and the jump button has been released")]
    public float lowJumpFallMultiplier = 1;

    public UnityEvent OnJumped = null;

    public override void Execute(EButtonAction typeOfAction)
    {
        if (typeOfAction==EButtonAction.DOWN && charState.charCtrl.isGrounded /*&& !charState.IsAttacking*/)
        {
            charState.IsUsingPower = true;
            charState.moveDirection.y = jumpSpeed;
            charState.charAnimator.SetTrigger(AnimatorParameters.Jump);

            if(OnJumped!=null)
            {
                OnJumped.Invoke();
            }
        }
        else if (!charState.charCtrl.isGrounded)
        {
            if (charState.moveDirection.y > 0 && typeOfAction==EButtonAction.NONE)
            {
                charState.moveDirection.y += Physics2D.gravity.y * lowJumpFallMultiplier * Time.deltaTime;
            }
            else
            {
                charState.moveDirection.y += Physics2D.gravity.y * fallMultiplier * Time.deltaTime;
            }
        }
        else
        {
            // is grounded and jump was not pressed, clean vertical movement to 0
            charState.moveDirection.y = -0.1f; //-0.1 should fix missing grounded frames
            charState.IsUsingPower = false;
        }
    }

}
