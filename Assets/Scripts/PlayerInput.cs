﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum EButtonAction
{
    NONE=0,
    DOWN=1,
    PRESSED=2,
    UP=3
}

[Serializable]
public class ButtonActionEvent: UnityEvent<EButtonAction>
{ }

public class PlayerInput : MonoBehaviour
{


    public ButtonActionEvent OnPower;
    public ButtonActionEvent OnMoveLeft;
    public ButtonActionEvent OnMoveRight;
    public ButtonActionEvent OnAttack;
    

    public void Update()
    {

        if(Input.GetAxisRaw("Debug")!=0)
        {
            Debug.Log("Pressed axis");
        }

        if (Input.GetButton("Debug"))
        {
            Debug.Log("Pressed button");
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }

        TryCallActionForButtonName(OnPower, "Power");
        //TryCallActionForButtonName(OnMoveRight, "MoveRight");
        TryCallActionForAxisName(OnMoveRight, "MoveRight");
        //TryCallActionForButtonName(OnMoveLeft, "MoveLeft");
        TryCallActionForAxisName(OnMoveLeft, "MoveLeft");
        TryCallActionForButtonName(OnAttack, "Attack");
        //TryCallActionForButtonName(OnDodge, "Dodge");

    }

    private void TryCallActionForButtonName(UnityEvent<EButtonAction> action, string buttonName )
    {
        EButtonAction typeOfButtonAction = EButtonAction.NONE;
        if (Input.GetButtonDown(buttonName))
        {
            typeOfButtonAction = EButtonAction.DOWN;
        }
        else if (Input.GetButtonUp(buttonName))
        {
            typeOfButtonAction = EButtonAction.UP;
        }
        else if (Input.GetButton(buttonName))
        {
            typeOfButtonAction = EButtonAction.PRESSED;
        }

        if(action != null/* && typeOfButtonAction!=EButtonAction.NONE*/)
        {
            action.Invoke(typeOfButtonAction);
        }
    }

    private void TryCallActionForAxisName(UnityEvent<EButtonAction> action, string axisName)
    {
        if(action!=null && Input.GetAxis(axisName) > 0)
        {
            action.Invoke(EButtonAction.PRESSED);
        }
    }
}

