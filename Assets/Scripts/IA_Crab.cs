﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterState))]
public class IA_Crab : IAActionSender
{

    public Transform target = null;

    public float distanceFromTargetToKeepMoving = 5f;
    //public float minimumDistanceFromTargetAllowed = 2f;
    //public float deltaTimeToAttemptAttackAction = 1f;
    public int numberOfBlocksToTriggerAttack = 2;

    //private float timeProgressionAttack = 0;
    private int progressionNumberOfBlocksToTriggerAttack = 100; 

    public void Update()
    {
        if(target)
        {
            ProcessIA();
        }
        else
        {
            // Target was lost, stop acting
            this.enabled = false;
        }
       
    }

    public override void ProcessIA()
    {

        //if(WaitForAfterDashPause())
        //{
        //    return;
        //}

        LookTowardsTarget();

        //if (!UpdateAttackRoutine())
        //{
        //    UpdateMoveRoutine();
        //}
        
        if(!UpdateMoveRoutine())
        {
            UpdateAttackRoutine();
        }
    }

    //private bool WaitForAfterDashPause()
    //{
    //    if(charState.IsUsingPower)
    //    {
    //        timeProgressionAfterDashPause = 0;
    //    }
    //    else
    //    {
    //        timeProgressionAfterDashPause += Time.deltaTime;
    //    }

        
    //    return timeProgressionAfterDashPause < timeForAfterDashPause;        

    //}

    private bool UpdateMoveRoutine()
    {
        if( transform.position.x - 0 > 0 && Vector3.Distance(target.position, transform.position) > distanceFromTargetToKeepMoving)//distance from game center, if on left is negative
        {
            if (OnMoveLeft != null && charState.LookingDirection==Vector3.left)
            {
                OnMoveLeft.Invoke(EButtonAction.PRESSED);
                return true;
            }
            else if (OnMoveRight != null && charState.LookingDirection == Vector3.right)
            {
                OnMoveRight.Invoke(EButtonAction.PRESSED);
                return true;
            }

        }

        return false;
    }

    private void UpdateAttackRoutine()
    {

        if(progressionNumberOfBlocksToTriggerAttack < numberOfBlocksToTriggerAttack)
        {
            if(OnPower != null)
            {
                OnPower.Invoke(EButtonAction.PRESSED);
            }
        }
        else
        {
            if (OnAttack != null)
            {
                OnAttack.Invoke(EButtonAction.DOWN);
                progressionNumberOfBlocksToTriggerAttack = 0;
            }
        }

    }

    public void LookTowardsTarget()
    {
        float lookDirection = target.position.x - transform.position.x;
        if(lookDirection!=0)
        {
            charState.LookingDirection = Vector3.right * (lookDirection / Mathf.Abs(lookDirection));
        }
    }

    public void IncreaseCounterOfBlockedAttacks()
    {
        ++progressionNumberOfBlocksToTriggerAttack;
    }
}
