﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableColliderWhenTargetsLookingSameDirection : MonoBehaviour
{
    public Transform target_A = null;
    public Transform target_B = null;

    public Collider coll = null;

    private void Update()
    {
        coll.enabled = target_A.forward.normalized == target_B.forward.normalized;
    }
}
