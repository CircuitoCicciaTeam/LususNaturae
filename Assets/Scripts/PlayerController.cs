﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [Header("Settings")]

    public float moveSpeed = 1;
    public float jumpSpeed = 1;
    [Tooltip("Affects gravity after reaching the top peack of the jump")]
    public float fallMultiplier = 1;
    [Tooltip("Affects gravity while the jump is going to the peack and the jump button has been released")]
    public float lowJumpFallMultiplier = 1;
    public float rotationSpeed = 20;

    public float chargeAttackTime = 1;

    [Header("Debug")]
    public CharacterController charCtrl = null;
    public Vector3 moveDirection = Vector3.zero;
    public bool isAttacking = false;

    private Vector3 lookingDirection = Vector3.right;

    // Use this for initialization
    void Start ()
    {
        charCtrl = GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        moveDirection.x = 0;
        moveDirection.z = 0;

        //MOVE
        if (Input.GetKey(KeyCode.RightArrow) && !isAttacking)
        {
            moveDirection.x += moveSpeed * Time.deltaTime;
            lookingDirection = Vector3.right;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && !isAttacking)
        {
            moveDirection.x -= moveSpeed * Time.deltaTime;
            lookingDirection = Vector3.left;
        }

        //JUMP
        if (Input.GetKeyDown(KeyCode.UpArrow) && charCtrl.isGrounded && !isAttacking)
        {
            moveDirection.y = jumpSpeed;
        }
        else if (!charCtrl.isGrounded)
        {
            if (moveDirection.y > 0 && !Input.GetKey(KeyCode.UpArrow))
            {
                moveDirection.y += Physics2D.gravity.y * lowJumpFallMultiplier * Time.deltaTime;
            }           
            else
            {
                moveDirection.y += Physics2D.gravity.y * fallMultiplier * Time.deltaTime;
            }
        }
        else
        {
            // is grounded and jump was not pressed, clean vertical movement to 0
            moveDirection.y = -0.1f; //-0.1 should fix missing grounded frames
        }

        //ATTACK
        if(Input.GetKeyDown(KeyCode.Z) && !isAttacking)
        {
            StartCoroutine(CoroutineAttack());
        }

        charCtrl.Move(moveDirection); //Apply Movement with collision

        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookingDirection,Vector3.up), rotationSpeed*Time.deltaTime);
    }

    private IEnumerator CoroutineAttack()
    {
        isAttacking = true;

        Debug.DrawLine(transform.position, transform.position + Vector3.up*3, Color.red, 1);

        yield return new WaitForSeconds(chargeAttackTime);

        Debug.DrawLine(transform.position, transform.position + Vector3.up*3, Color.green, 1);

        yield return new WaitForSeconds(0.3f);
        Debug.DrawLine(transform.position, transform.position + lookingDirection*3, Color.green, 1);


        isAttacking = false;
    }

}
