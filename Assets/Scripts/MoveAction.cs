﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveAction : CharacterAction
{
    [Header("Settings")]
    public float moveSpeed = 1;
    public float rotationSpeed = 180;

    public override void Execute(EButtonAction typeOfAction)
    {
        throw new NotImplementedException();
    }

    public void MoveRight(EButtonAction typeOfAction)
    {
        if (typeOfAction != EButtonAction.NONE)
        {
            charState.moveDirection.x += moveSpeed * Time.deltaTime;
            charState.LookingDirection = Vector3.right;

        }
    }

    public void MoveLeft(EButtonAction typeOfAction)
    {
        if (typeOfAction != EButtonAction.NONE)
        {
            charState.moveDirection.x -= moveSpeed * Time.deltaTime;
            charState.LookingDirection = Vector3.left;
        }
    }

    public void Update()
    {

        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(charState.LookingDirection, Vector3.up), rotationSpeed * Time.deltaTime);
    }
}
