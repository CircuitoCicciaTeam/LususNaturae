﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[DefaultExecutionOrder(10)]
public class CharacterState : MonoBehaviour
{
    [SerializeField]
    private Vector3 lookingDirection = Vector3.right;

    [Header("Debug")]
    public CharacterController charCtrl = null;
    public Animator charAnimator = null;
    public Vector3 moveDirection = Vector3.zero;
    public bool reloadLevelOnDeath = true;
    private bool isAttacking = false;
    private bool isUsingPower = false;
    private bool isDead = false;
    

    public Vector3 LookingDirection
    {
        get
        {
            return lookingDirection;
        }

        set
        {
            if (!IsAttacking && !isUsingPower)//do not turn if attackking or using powers
            {
                lookingDirection = value;
                charAnimator.SetBool(AnimatorParameters.LookingLeft, lookingDirection == Vector3.left);
            }
        }
    }

    public bool IsAttacking
    {
        get
        {
            return isAttacking;
        }

        set
        {
            isAttacking = value;
            if (isAttacking)
            {
                charAnimator.SetTrigger(AnimatorParameters.Attack);
            }
        }
    }

    public bool IsUsingPower
    {
        get
        {
            return isUsingPower;
        }

        set
        {
            isUsingPower = value;
        }
    }

    void Awake ()
    {
        charCtrl = GetComponent<CharacterController>();
        charAnimator = GetComponent<Animator>();
	}

    private void Update()
    {
        //Use the state to update movement and rotation, these are changed from CharacterActions
        charCtrl.Move(moveDirection);

        if (charAnimator)
        {
            charAnimator.SetBool(AnimatorParameters.Idle, moveDirection.x == 0);
            charAnimator.SetBool(AnimatorParameters.Grounded, charCtrl.isGrounded);
        }

        moveDirection.x = 0;
        moveDirection.z = 0;

        //hack to keep chars on same depth
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    public void Die()
    {
        isDead = true;
        charAnimator.enabled = false;

        if(reloadLevelOnDeath)
        {
            LoadNextScene.TryLoadingScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }
    }

}
