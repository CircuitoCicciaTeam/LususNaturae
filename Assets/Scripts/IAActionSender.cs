﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IAActionSender : MonoBehaviour {

    public ButtonActionEvent OnPower;
    public ButtonActionEvent OnMoveLeft;
    public ButtonActionEvent OnMoveRight;
    public ButtonActionEvent OnAttack;

    public CharacterState charState = null;

    private void Awake()
    {
        charState = GetComponent<CharacterState>();
    }

    public abstract void ProcessIA();



}
