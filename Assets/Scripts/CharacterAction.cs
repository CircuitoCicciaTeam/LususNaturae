﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//maybe add state of character
[RequireComponent(typeof(CharacterState))]
public abstract class CharacterAction : MonoBehaviour
{
    [Header("Debug")]
    public CharacterState charState = null;

    void Awake()
    {
        charState = GetComponent<CharacterState>();
    }

    public abstract void Execute(EButtonAction typeOfAction);
}
