﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendDamageAction : MonoBehaviour {

    public int rawDamage = 100;

    private void OnTriggerEnter(Collider other)
    {
        ReceiveDamageAction compRecieveDamage = other.GetComponent<ReceiveDamageAction>();
        if (compRecieveDamage)
        {
            compRecieveDamage.ApplyDamage(rawDamage);
            compRecieveDamage.StartCoroutine(compRecieveDamage.CoroutineFlickerAndDisable());
            
        }
    }
}
