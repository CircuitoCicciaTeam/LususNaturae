﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAction : CharacterAction
{
    [Header("Settings")]
    public Collider damageColliderToEnable = null;

    private bool attackAlreadyConnected = false;

    public bool AttackAlreadyConnected
    {
        get
        {
            return attackAlreadyConnected;
        }

        set
        {
            attackAlreadyConnected = value;
        }
    }

    public override void Execute(EButtonAction typeOfAction)
    {
        if(typeOfAction==EButtonAction.DOWN && !charState.IsAttacking)
        {
            charState.IsAttacking = true;            
        }
    }

    public void EnableAttackDamage()
    {
        damageColliderToEnable.enabled = true;
    }

    public void StopAttackDamage()
    {
        damageColliderToEnable.enabled = false;
    }

    public void AttackConnected()
    {
        AttackAlreadyConnected = true;
    }

    public void FinalizeAttack()
    {
        StopAttackDamage();
        charState.IsAttacking = false;
    }

    

}
