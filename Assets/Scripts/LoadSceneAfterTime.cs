﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSceneAfterTime : MonoBehaviour
{
    public float timeBeforeLoadingScene = 10f;
    public string sceneToLoad = "";

    private void Start()
    {
        StartCoroutine(CouroutineLoadSceneAfterTime());
    }

    private IEnumerator CouroutineLoadSceneAfterTime()
    {
        yield return new WaitForSeconds(timeBeforeLoadingScene);

        LoadNextScene.TryLoadingScene(sceneToLoad);
    }
}
