%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: HumanUpperBody
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Cube
    m_Weight: 1
  - m_Path: HumanArma
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/IKArm_L
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKArm_L/IKArm_L_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKArm_R
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKArm_R/IKArm_R_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKLeg_L
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKLeg_L/IKLeg_L_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKLeg_R
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKLeg_R/IKLeg_R_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKTorso
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/IKTorso/IKTorso_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_L.001
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_L.001/Leg_L.002
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_L.001/Leg_L.002/Leg_L.003
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_L.001/Leg_L.002/Leg_L.003/Leg_L.004
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_L.001/Leg_L.002/Leg_L.003/Leg_L.004/Leg_L.004_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_R.001
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_R.001/Leg_R.002
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_R.001/Leg_R.002/Leg_R.003
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_R.001/Leg_R.002/Leg_R.003/Leg_R.004
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Leg_R.001/Leg_R.002/Leg_R.003/Leg_R.004/Leg_R.004_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002/pagne.003
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002/pagne.003/pagne.004
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002/pagne.003/pagne.004/pagne.005
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002/pagne.003/pagne.004/pagne.005/pagne.006
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002/pagne.003/pagne.004/pagne.005/pagne.006/pagne.007
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002/pagne.003/pagne.004/pagne.005/pagne.006/pagne.007/pagne.008
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/pagne.001/pagne.002/pagne.003/pagne.004/pagne.005/pagne.006/pagne.007/pagne.008/pagne.008_end
    m_Weight: 0
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002/Arm.002_L.003
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002/Arm.002_L.003/Arm.002_L.004
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002/Arm.002_L.003/Arm.002_L.004/Arm.002_L.005
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002/Arm.002_L.003/Arm.002_L.004/Arm.002_L.005/Arm.002_L.005_end
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002/Arm.002_L.003/Arm.002_L.006
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002/Arm.002_L.003/Arm.002_L.006/Arm.002_L.007
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_L/Arm.002_L.001/Arm.002_L.002/Arm.002_L.003/Arm.002_L.006/Arm.002_L.007/Arm.002_L.007_end
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002/Arm.002_R.003
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002/Arm.002_R.003/Arm.002_R.004
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002/Arm.002_R.003/Arm.002_R.004/Arm.002_R.005
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002/Arm.002_R.003/Arm.002_R.004/Arm.002_R.005/Arm.002_R.005_end
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002/Arm.002_R.003/Arm.002_R.006
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002/Arm.002_R.003/Arm.002_R.006/Arm.002_R.007
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Arm.002_R/Arm.002_R.001/Arm.002_R.002/Arm.002_R.003/Arm.002_R.006/Arm.002_R.007/Arm.002_R.007_end
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Torso.003
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Torso.003/Torso.004
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Torso.003/Torso.004/Head
    m_Weight: 1
  - m_Path: HumanArma/UBERROOT/ROOT/Torso.001/Torso.002/Torso.003/Torso.004/Head/Head_end
    m_Weight: 1
